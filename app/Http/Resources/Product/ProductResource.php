<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'produkts' => $this->product_name,
            'apraksts' => $this->product_detail,
            'cena' => $this->product_price,
            'cena_ar_atlaidi' => round(( 1 - ($this->product_discount/100)) * $this->product_price,2),
            'daudzums_pieejams' => $this->product_in_stock == 0 ? 'Izpirkta' : $this->product_in_stock,
            'atlaide' => $this->product_discount,
            'vertejums' => $this->reviews->count() > 0 ? round($this->reviews->sum('review_rating')/$this->reviews->count(),2) : ('Nav vertejuma'),
            'href' => [
                'atsauces' => route('reviews.index', $this->id)
            ]
        ];
    }
}
