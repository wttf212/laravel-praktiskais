<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\Resource;


class ProductCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'produkts' => $this->product_name,
            'atlaide' => $this->product_discount,
            'cena_ar_atlaidi' => round(( 1 - ($this->product_discount/100)) * $this->product_price,2),
            'vertejums' => $this->reviews->count() > 0 ? round($this->reviews->sum('review_rating')/$this->reviews->count(),2) : ('Nav vertejuma'),
            'href' => [
                'link' => route('products.show', $this->id)
            ]
        ];
    }
}
