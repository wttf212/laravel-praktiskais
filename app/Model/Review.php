<?php

namespace App\Model;

use App\Model\Product;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
      'customer_name','review_rating','review_text'  
    ];
    public function product(){
        return $this->belongsTo(Product::class);
    }
}
