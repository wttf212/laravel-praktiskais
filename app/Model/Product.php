<?php

namespace App\Model;

use App\Model\Review;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_name','product_detail','product_in_stock','product_price','product_discount',
    ];
    

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
