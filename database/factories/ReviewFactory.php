<?php

use App\Model\Product;
use Faker\Generator as Faker;

$factory->define(App\Model\Review::class, function (Faker $faker) {
    return [
        'product_id' => function(){
           return Product::all()->random();
        },
            
        'customer_name' => $faker->name,
        'review_text' => $faker->paragraph,
        'review_rating' => $faker->numberBetween(0,5)
    ];
});
